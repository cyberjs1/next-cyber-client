import { configureStore } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import authSlice, { authReducer } from "./auth-slice";

const store = () =>
  configureStore({
    reducer: {
      [authSlice.name]: authReducer,
    },
    devTools: true,
  });

export const wrapper = createWrapper(store);
