import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  authState: false,
};

// Actual Slice
export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    // Action to set the authentication status
    setAuthState(state, action) {
      state.authState = action.payload;
    },
  },
});

export const { reducer: authReducer, actions } = authSlice;
export const { setAuthState } = actions;
export default authSlice;
