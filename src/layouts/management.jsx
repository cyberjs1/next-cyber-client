import Link from "next/link";

function ManagementLayout({ children, sideBarActive = "courses" }) {
  return (
    <div className="container.is-fullhd management_layout__wrapper">
      <header className="management_layout__header">
        <img src="/logoCyber.png" width="97px" height="30px" alt="cyber-logo" />
        <h2 className="">Courses Management</h2>
        <div className="management_layout__user-box">
          <div className="management_layout__user-name">
            <span>
              <i class="fa fa-user has-text-primary"></i>
            </span>
            Chinhhoang
          </div>
        </div>
        <div className="management_layout__notification-icon">
          <i class="fa fa-bell has-text-primary"></i>
        </div>
        <div className="management_layout__back-to-home">Back to home</div>
      </header>
      <div className="management_layout__container">
        <div className="management_layout__sidebar">
          <div class="menu">
            <p class="menu-label">General</p>
            <ul class="menu-list">
              <li>
                <Link
                  href={"/managements/courses"}
                  className={sideBarActive == "courses" ? "is-active" : ""}
                >
                  Courses management
                </Link>
              </li>
              {/* <li>
                <a>Accounts management</a>
              </li> */}
            </ul>
            <p class="menu-label">Human</p>
            <ul class="menu-list">
              <li>
                <a>Chat</a>
              </li>
              <li>
                <Link
                  href={"/managements/accounts"}
                  className={sideBarActive == "accounts" ? "is-active" : ""}
                >
                  Account management
                </Link>
              </li>
            </ul>
            <p class="menu-label">Transactions</p>
            <ul class="menu-list">
              <li>
                <a>Orders management</a>
              </li>
              <li>
                <a>Statistical</a>
              </li>
            </ul>
          </div>
        </div>
        <div className="management_layout__content">{children}</div>
      </div>
    </div>
  );
  // class="container.is-fullhd"
}

export default ManagementLayout;
