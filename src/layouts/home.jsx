import RequireAuth from "@/pages/RequireAuth";
import { useEffect, useState } from "react";
import PopupMessage from "@/components/popup-message";
import Nav from "@/components/navigation";
import Link from "next/link";
import { useRouter } from "next/router";
import Footer from "@/components/footer";

export default function HomeLayout({ children }) {
    const { asPath } = useRouter();
    const [select, setSelect] = useState(['is-active', '', '', '', '']);
    function setSelected(index) {
        const newNavItem = select.map((item, i) => {
            let navArray = []
            if (i === index) {
                return [...navArray, 'is-active'];
            }
            return navArray;
        });
        setSelect(newNavItem);
    }
    useEffect(() => {
        if (asPath.match('/course*')) {
            setSelected(3)
        }
        if (asPath.match('/messenger*')) {
            setSelected(4)
        }
        else {
            return;
        }
    }, [asPath]);
    return (
        <RequireAuth>
            <Nav />
            <div className="section m-0 p-0">
                <div className="columns is-vcentered m-0 p-0">
                    <div className="column is-12 p-0 m-0">
                        <div className="tabs-wrapper is-flex is-flex-direction-row is-12 m-0 p-0">
                            <div className="is-flex sd-tab is-1">
                                <ul className="is-flex-direction-column mx-5 my-5 h-100">
                                    <li className={select[0] + " m-1"}>
                                        <Link className="button l-bt" href="/">
                                            <span className="icon">
                                                <i className="fa fa-home icons-large"></i>
                                            </span>
                                        </Link>
                                    </li>
                                    <li className={select[1] + " m-1"}>
                                        <Link className="button l-bt" href="#">
                                            <span className="icon">
                                                <i className="fa fa-road icons-large"></i>
                                            </span>

                                        </Link></li>
                                    <li className={select[2] + " m-1"}>
                                        <Link className="button l-bt" href="#">
                                            <span className="icon">
                                                <i className="fa fa-laptop icons-large"></i>
                                            </span>
                                        </Link></li>
                                    <li className={select[3] + " m-1"}>
                                        <Link className="button l-bt" href="/course/2">
                                            <span className="icon">
                                                <i className="fa fa-lightbulb-o icons-large"></i>
                                            </span>
                                        </Link></li>
                                    <li className={select[4] + " m-1"}>
                                        <Link className="button l-bt" href="/messenger">
                                            <span className="icon">
                                                <i className="fa fa-comments icons-large"></i>
                                            </span>
                                        </Link></li>
                                </ul>
                            </div>
                            <div className="content-wrap  is-block is-12" style={{ width: '100%' }}>
                                <div className={"tab-content p-0 "} style={{ height: "95vh", overflow: "scroll" }}>
                                    {children}
                                    <Footer/>
                                </div>
                            </div>
                            {asPath.match('/messenger*') ? null : <PopupMessage />}
                        </div>
                    </div>
                </div>
            </div >
        </RequireAuth>
    );
}