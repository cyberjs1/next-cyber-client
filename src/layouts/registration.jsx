
function RegistrationLayout({ children }) {
    return (

        <div className="registration_layout__wrapper">
            <div className="registration_layout__inner">
                <div className="registration_layout__logo">
                    <img src="../logoCyber.png" width="200" height="100" />
                </div>
                <div className="registration_layout__content">
                    {children}
                </div>


            </div>
        </div>
    );
}

export { RegistrationLayout };