import CourseManagement from "@/components/course-management";
import ManagementLayout from "@/layouts/management";

function CoursesManagementPage() {
  return <ManagementLayout>{<CourseManagement />}</ManagementLayout>;
}
export default CoursesManagementPage;
