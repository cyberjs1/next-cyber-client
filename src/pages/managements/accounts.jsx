import ManagementLayout from "@/layouts/management";
import AccountsManagement from "@/components/accounts-management";

function AccountsManagementPage() {
  return (
    <ManagementLayout sideBarActive="accounts">
      <AccountsManagement></AccountsManagement>
    </ManagementLayout>
  );
}

export default AccountsManagementPage;
