import RequireAuth from "./RequireAuth"
export default function About() {
    return (
        <RequireAuth>
            <h1><b>About</b></h1>
        </RequireAuth>
    )
}