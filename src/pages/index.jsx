import Carousel from "@/components/home/Carousel";
import PreviewCourse from "@/components/home/PreviewCourse";
import HomeLayout from "@/layouts/home";

export default function HomePage() {
    return (
        <HomeLayout>
            <title>Home</title>
            <Carousel />
            <PreviewCourse />
        </HomeLayout>
    )
}