import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { isAuthenticated } from '../utils/auth';

const RequireAuth = ({ children }) => {
  const router = useRouter();

  useEffect(() => {
    if (!isAuthenticated() && router.pathname !== '/home' && router.pathname !== '/') {
      router.push('/login');
    }
  }, []);

  return children;
};

export default RequireAuth;
