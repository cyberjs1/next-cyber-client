import { RegistrationLayout } from "@/layouts";
import Link from "next/link";
import ReCAPTCHA from "react-google-recaptcha"


function ForgotPassword() {
    return (
        <RegistrationLayout>
            <div className="forgot_password__wrapper">
                <h2 class="title is-2mt-0 mb-6">Forgot Password</h2>
                <div>
                    <div class="field mb-6">
                        <label class="label">Enter Your Email</label>
                        <p class="control has-icons-left">
                            <input class="input  is-rounded" type="email" placeholder="thihaonguyen1309@gmail.com" />
                            <span class="icon is-small is-left">
                                <i class="fa fa-envelope"></i>
                            </span>
                        </p>
                    </div>
                    <div className="forgot_password__capcha">
                        <ReCAPTCHA sitekey="hk" />
                    </div>

                    <div class="field">
                        <p class="control">
                            <button class="button is-success is-fullwidth is-rounded">
                                Send
                            </button>
                        </p>
                    </div>
                </div>

                <div class="mt-5">
                <span class="icon is-small is-left has-text-primary">
                        <i class="fa fa-angle-left"></i>
                    </span>
                    <Link href="/auth/login" class="has-text-primary">Back</Link>
                </div>
            </div>

        </RegistrationLayout>
    );
}

export default ForgotPassword;