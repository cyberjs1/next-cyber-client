import { RegistrationLayout } from "@/layouts";
import Link from "next/link";


function ChangePassword() {
    return (
        <RegistrationLayout>
            <div className="forgot_password__wrapper">
                <h2 class="title is-2 mb-6">Change Password</h2>
                <div class="field">
                    <label class="label">New Password</label>
                    <p class="control has-icons-left">
                        <input class="input  is-rounded" type="password" placeholder="Password" />
                        <span class="icon is-small is-left">
                            <i class="fa fa-lock"></i>
                        </span>
                    </p>
                </div>
                <div class="field">
                    <label class="label">Confirm Password</label>
                    <p class="control has-icons-left">
                        <input class="input  is-rounded" type="password" placeholder="Password" />
                        <span class="icon is-small is-left">
                            <i class="fa fa-lock"></i>
                        </span>
                    </p>
                </div>

                <div class="field mt-6">
                    <p class="control">
                        <button class="button is-success is-fullwidth is-rounded">
                            Change Password
                        </button>
                    </p>
                </div>


                <div class="mt-6">
                    <span class="icon is-small is-left has-text-primary">
                        <i class="fa fa-angle-left"></i>
                    </span>
                    <Link href="/auth/login" class="has-text-primary">Back</Link>
                </div>
            </div>




        </RegistrationLayout>
    );
}

export default ChangePassword;