import { RegistrationLayout } from "@/layouts";
import Link from "next/link";

import { useFormik } from "formik";
import * as Yup from "yup";


function RegisterPage() {

    const formik = useFormik({
        initialValues: {
            full_name: "",
            email: "",
            password: "",
        },
        validationSchema: Yup.object({
            full_name: Yup.string()
                .min(2, "Mininum 2 characters")
                .max(15, "Maximum 15 characters")
                .required("Required!"),
            email: Yup.string()
                .email("Invalid email format")
                .required("Required!"),
            password: Yup.string()
                .min(8, "Minimum 8 characters")
                .required("Required!"),

        }),
        onSubmit: values => {
            alert(JSON.stringify(values, null, 2));
        }
    });

    return (
        <RegistrationLayout>
            <div>
                <div class="field">
                    <label class="label m-0">Full name</label>
                    <div class="control">
                        <input class="input is-rounded "
                            type="text"
                            name="full_name"
                            placeholder="Nguyễn Thị Hảo"
                            value={formik.values.full_name}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.full_name && formik.touched.full_name && (
                            <p>{formik.errors.full_name}</p>
                        )}
                    </div>
                </div>
                <div class="field">
                    <label class="label m-0">Email</label>
                    <p class="control has-icons-left">
                        <input class="input is-rounded "
                            type="email"
                            name="email"
                            placeholder="thihaonguyen1309@gmail.com"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.email && formik.touched.email && (
                            <p>{formik.errors.email}</p>
                        )}
                        <span class="icon is-small is-left">
                            <i class="fa fa-envelope"></i>
                        </span>
                    </p>
                </div>
                <div class="field">
                    <label class="label m-0">Password</label>
                    <p class="control has-icons-left">
                        <input class="input is-rounded "
                            type="password"
                            name="password"
                            placeholder="Password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.password && formik.touched.password && (
                            <p>{formik.errors.password}</p>
                        )}
                        <span class="icon is-small is-left">
                            <i class="fa fa-lock"></i>
                        </span>
                    </p>
                </div>
                <div class="field">
                    <label class="label m-0">Confirm Password</label>
                    <p class="control has-icons-left">
                        <input class="input  is-rounded" type="password" placeholder="Password" />
                        <span class="icon is-small is-left">
                            <i class="fa fa-lock"></i>
                        </span>
                    </p>
                </div>
                <label class="checkbox mb-2">
                    <input type="checkbox" />
                     Tôi đã đọc và chấp nhận<Link href="/" class="has-text-primary ml-1">điều khoản của Cyber</Link>
                </label>
                <div class="field mb-1">
                    <p class="control">
                        <button class="button is-success is-rounded is-fullwidth">
                            Register
                        </button>
                    </p>
                </div>
                <p class="has-text-primary has-text-centered mb-2">or continue with</p>

                <div class="hero is-flex-direction-row is-justify-content-center is-align-items-center">
                    <button class="button pl-6 pr-6">
                        <span class="icon">
                            <i class="fa fa-google"></i>
                        </span>
                        <span>Google</span>
                    </button>
                </div>

                <div class="has-text-centered mt-2">
                    <span>Don’t have a account?</span>
                    <Link href="/auth/login" class="has-text-danger ml-1">Sign in</Link>
                </div>
            </div>

        </RegistrationLayout>
    );
}

export default RegisterPage;