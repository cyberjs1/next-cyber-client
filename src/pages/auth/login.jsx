import { RegistrationLayout } from "@/layouts";
import Link from "next/link";
import { useFormik } from "formik";
import * as Yup from "yup";


function LoginPage() {

    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .email("Invalid email format")
                .required("Required!"),
            password: Yup.string()
                .min(8, "Minimum 8 characters")
                .required("Required!"),
        }),
        onSubmit: values => {
            alert(JSON.stringify(values, null, 2));
        }
    });


    return (
        <RegistrationLayout>
            <div>
                <div class="field">
                    <label class="label">Email</label>
                    <p class="control has-icons-left">

                        <input class="input  is-rounded"
                            name="email" type="email"
                            placeholder="thihaonguyen1309@gmail.com"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.email && formik.touched.email && (
                            <p>{formik.errors.email}</p>
                        )}
                        <span class="icon is-small is-left">
                            <i class="fa fa-envelope"></i>
                        </span>
                    </p>
                </div>
                <div class="field mt-4">
                    <label class="label">Password</label>
                    <p class="control has-icons-left">
                        <input class="input  is-rounded"
                            name="password" type="password"
                            placeholder="Password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.password && formik.touched.password && (
                            <p>{formik.errors.password}</p>
                        )}
                        <span class="icon is-small is-left">
                            <i class="fa fa-lock"></i>
                        </span>
                    </p>
                </div>
                <div class="hero is-flex-direction-row is-justify-content-space-between is-align-items-center pb-3">
                    <label class="checkbox">
                        <input type="checkbox" />
                        Remember me
                    </label>
                    <p><Link href="/auth/forgot-password" class="has-text-primary">Forgot password?</Link></p>

                </div>

                <div class="field mt-5 mb-3">
                    <p class="control">
                        <button class="button is-success is-fullwidth is-rounded">
                            Login
                        </button>
                    </p>
                </div>
                <p class="has-text-primary has-text-centered mb-4">or continue with</p>

                <div class="hero is-flex-direction-row is-justify-content-center is-align-items-center">
                    <button class="button pl-6 pr-6">
                        <span class="icon">
                            <i class="fa fa-google"></i>
                        </span>
                        <span>Google</span>
                    </button>
                </div>
                <div class="has-text-centered mt-5">
                    <span>Don’t have a account?</span>
                    <Link href="/auth/register" class="has-text-danger ml-1">Sign up</Link>
                </div>
            </div>




        </RegistrationLayout>
    );
}

export default LoginPage;