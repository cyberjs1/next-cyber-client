import Chat from "@/components/chat";
import Conversation from "@/components/conversation";
import HomeLayout from "@/layouts/home";
import { useState } from "react";
import DetailConversation from "./messenger/detail-conversation";

export default function Messenger() {
    const [openPopup, setOpenPopup] = useState(false);
    const [openChat, setOpenChat] = useState(false);
    const [data, setData] = useState([
        { name: "John Doe 0", img: "/user/avatar.png" },
        { name: "John Doe 1", img: "/user/avatar.png" },
        { name: "John Doe 2", img: "/user/avatar.png" },
        { name: "John Doe 3", img: "/user/avatar.png" },
        { name: "John Doe 4", img: "/user/avatar.png" },
        { name: "John Doe 5", img: "/user/avatar.png" },
        { name: "John Doe 5", img: "/user/avatar.png" },
        { name: "John Doe 6", img: "/user/avatar.png" },
        { name: "John Doe 7", img: "/user/avatar.png" },
        { name: "John Doe 8", img: "/user/avatar.png" },
        { name: "John Doe 9", img: "/user/avatar.png" },
        { name: "John Doe 10", img: "/user/avatar.png" },
        { name: "John Doe 11", img: "/user/avatar.png" },
        { name: "John Doe 11", img: "/user/avatar.png" },
        { name: "John Doe 11", img: "/user/avatar.png" },
        { name: "John Doe 11", img: "/user/avatar.png" },
        { name: "John Doe 11", img: "/user/avatar.png" },
    ]);
    const [indexUser, setIndexUser] = useState(null);
    return (
        <HomeLayout>
            <title>Cyber JS - Mesenger</title>
            <div className="is-flex">
                <div className="conversation-list">
                    {
                        data.map((item, index) => {
                            return (
                                <div key={index}>
                                    <Conversation name={item.name} img={item.img} setOpenChat={setOpenChat} index={index} setIndexUser={setIndexUser} />
                                </div>
                            )
                        })}
                </div>
                <div className="chat-user">
                    {
                        indexUser !== null ?
                            <Chat openChat={openChat} setOpenChat={setOpenChat} user={data[indexUser]} isPopup={false} />
                            :
                            <div className="null-chat">
                                <b>Select a conversation to start messaging</b>
                            </div>
                    }
                </div>
                {indexUser !== null ?
                    <div className="detail-conversation">
                        <DetailConversation />
                    </div>
                    : null
                }
            </div>
        </HomeLayout>
    )
}