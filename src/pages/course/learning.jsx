import { useState } from "react";
import DropdownLearning from "@/components/courses/dropdown-learning";
import NoteComponent from "@/components/courses/note";
import QuestionComponent from "@/components/courses/question";
import NoteOnlyComponent from "@/components/courses/note-only";

function CourseDetail() {

    const [showModal, setShowModal] = useState(false);
    const [showNoteModal, setShowNoteModal] = useState(false);
    const [showQuestionModal, setShowQuestionModal] = useState(false);

    return (
        <div className="course_detail__wrapper">
            <div className="has-background-primary course_detail__header">
                <div className="course_detail__header-left">
                    <button className="course_detail__header-left-btnback">
                        <span className="icon has-text-white">
                            <i className="fa fa-chevron-left"></i>
                        </span>
                    </button>
                    <img className="ml-2" src=".././logoCyber.png" width="80" height="50" alt="logo" />
                    <h6 className="title is-6 ml-3 has-text-white">HTML CSS từ Zero đến Hero</h6>
                </div>

                <div className="course_detail__header-right">
                    <span className="has-text-white">0/233 bài học</span>

                    <button className="icon-text"
                        onClick={() => setShowModal(true)}
                    >
                        <span className="icon has-text-white">
                            <i className="fa fa-sticky-note"></i>
                        </span>
                        <span className="has-text-white">Ghi chú</span>
                    </button>

                    {showModal &&
                        <NoteComponent />}

                    <button className="icon-text">
                        <span className="icon has-text-white">
                            <i className="fa fa-question-circle"></i>
                        </span>
                        <span className="has-text-white">Hướng dẫn</span>
                    </button>

                </div>
            </div>

            <div className="course_detail__body">

                <div className="course_detail__body-content">
                    <div className="course_detail__body-content-top">
                        <iframe className="has-ratio course_detail__body-video" src="https://www.youtube.com/embed/YE7VzlLtp-4" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div className="course_detail__body-content-bottom">
                        <div style={{ height: "50px", display: "flex", justifyContent: "space-between", alignItems: "flex-end", color: "black" }}>
                            <div><h1 className="title is-3">Header giỏ hàng trống CSS</h1></div>
                            <button className="button  is-light">
                                <span className="icon">
                                    <i className="fa fa-plus"></i>
                                </span>
                                <span
                                    onClick={() => setShowNoteModal(!showNoteModal)}
                                >Thêm ghi chú tại <b>{"00:00"}</b></span>
                            </button>
                        </div>

                        <div style={{ width: "100%", height: "30px", marginTop: "15px" }}>
                            <p>Cập nhật {"tháng 2 năm 2023"}</p>
                        </div>



                        <div className="course_detail__body-content-txtMade">
                            <p>Made with <i className="fa fa-heart has-text-danger"></i>  by Cyber team</p>
                        </div>
                        <button
                            style={{ width: "110px", height: "35px", position: "fixed", bottom: "65px", right: "25%" }}
                            onClick={() => setShowQuestionModal(true)}
                            className="button is-danger is-rounded">
                            <span className="icon">
                                <i className="fa fa-comments"></i>
                            </span>
                            <span>Hỏi đáp</span>
                        </button>

                    </div>


                </div>

                <div className="course_detail__body-sidebar">
                    <div className="body-sidebar-header">
                        <span>Nội dung khóa học</span>
                    </div>
                    <div className="body-sidebar-listlesson">
                        <DropdownLearning></DropdownLearning>
                        <DropdownLearning></DropdownLearning>
                        <DropdownLearning></DropdownLearning>
                        <DropdownLearning></DropdownLearning>


                    </div>

                </div>

            </div>

            <div className="course_detail__footer">

                <div className="course_detail__footer-group-btnaction">
                    <button className="button course_detail__footer-btnlesson">
                        <span className="icon">
                            <i className="fa fa-chevron-left"></i>
                        </span>
                        <span>Bài trước</span>
                    </button>
                    <button className="button is-primary is-outlined course_detail__footer-btnlesson">
                        <span>Bài tiếp theo</span>
                        <span className="icon">
                            <i className="fa fa-chevron-right"></i>
                        </span>
                    </button>
                </div>

                <div className="course_detail__footer-lesson-name">
                    <span className="mr-3">1. Bắt đầu </span>
                    <button className="course_detail__footer-btnnext">
                        <span className="icon">
                            <i className="fa fa-arrow-right"></i>
                        </span>
                    </button>

                </div>

            </div>
            {showNoteModal && <NoteOnlyComponent isActive={showNoteModal} setActiveModel={setShowNoteModal}></NoteOnlyComponent>}
            <NoteComponent isActive={showModal} setActiveModel={setShowModal}></NoteComponent>
            <QuestionComponent isActive={showQuestionModal} setActiveModel={setShowQuestionModal}></QuestionComponent>

        </div >
    );
}


export default CourseDetail;