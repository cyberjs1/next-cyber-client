
import HomeLayout from "@/layouts/home";
import { useRouter } from "next/router";
import { useRef, useState } from "react";


function CourseInfo() {
    const route = useRouter();
    return (
        <HomeLayout>
            <div className="course_info__wrapper">
                <div className="course_info__inner">
                    <div className="course_info__left">
                        <h3 className="title is-3">HTML CSS từ Zero đến Hero</h3>
                        <h6 className="subtitle is-6 mt-3">
                            Trong khóa này chúng ta sẽ cùng nhau xây dựng giao diện 2 trang web là The Band & Shopee.
                        </h6>
                        <div className="course_info__achieve">
                            <h6 className="title is-5 mb-3">Bạn sẽ học được gì?</h6>
                            <div className="course_info__achieve-body">
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Biết cách xây dựng giao diện web với HTML, CSS</span>
                                    </div>
                                </div>

                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Biết cách đặt tên className CSS theo chuẩn BEM</span>
                                    </div>
                                </div>
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Làm chủ Flexbox khi dựng bố cục website</span>
                                    </div>
                                </div>
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Biết cách tự tạo động lực cho bản thân</span>
                                    </div>
                                </div>
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Học được cách làm UI chỉn chu, kỹ tính</span>
                                    </div>
                                </div>
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Biết cách phân tích giao diện website</span>
                                    </div>
                                </div>
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Biết cách làm giao diện web responsive</span>
                                    </div>
                                </div>
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Sở hữu 2 giao diện web khi học xong khóa học</span>
                                    </div>
                                </div>
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Biết cách tự học những kiến thức mới</span>
                                    </div>
                                </div>
                                <div className="course_info__achieve-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Nhận chứng chỉ khóa học do Cyber cấp</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="course_info__content">
                            <h6 className="title is-5 mt-5">Nội dung khóa học</h6>
                            <div className="course_info__content-body">
                                <DropdownLesson></DropdownLesson>
                                <DropdownLesson></DropdownLesson>
                                <DropdownLesson></DropdownLesson>
                            </div>
                        </div>

                        <div className="course_info__require">
                            <h6 className="title is-5 mt-6 mb-3">Yêu cầu</h6>
                            <div className="course_info__require-body">
                                <div className="course_info__require-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Biết cách tự học những kiến thức mới</span>
                                    </div>
                                </div>
                                <div className="course_info__require-item">
                                    <div className="icon-text">
                                        <span className="icon has-text-danger">
                                            <i className="fa fa-check"></i>
                                        </span>
                                        <span>Nhận chứng chỉ khóa học do Cyber cấp</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="course_info__related">
                            <h6 className="title is-5 mt-6 mb-3">Khóa học liên quan</h6>
                            <div className="course_info__related-body">
                                <RelatedCourseItem></RelatedCourseItem>
                                <RelatedCourseItem></RelatedCourseItem>
                                <RelatedCourseItem></RelatedCourseItem>
                            </div>
                        </div>

                    </div>

                    <div className="course_info__right">
                        <div className="course_info__right-thumb">
                            <img className="course_info__right-image" src=".././logoCyber.png" alt="thumbnail" />
                            <div className="course_info__right-bubble">
                                <span className="icon has-text-white">
                                    <i className="fa fa-play-circle bubble-icon"></i>
                                </span>
                                <span>Xem giới thiệu khóa học</span>
                            </div>

                        </div>

                        <div className="course_info__right-content">
                            <h3 className="title is-3 mt-5 has-text-danger">Miễn phí</h3>
                            <button
                                onClick={() => route.push('/course/learning?id=123')}
                                className="button is-danger is-rounded mb-5 pl-6 pr-6">Đăng ký học</button>
                            <div className="course_info__right-content-basic">
                                <div className="icon-text mb-2 course_info__content-basic-item">
                                    <span className="icon has-text-danger mr-4">
                                        <i className="fa fa-globe"></i>
                                    </span>
                                    <span>Trình độ cơ bản</span>
                                </div>
                                <div className="icon-text mb-2 course_info__content-basic-item">
                                    <span className="icon has-text-danger mr-4">
                                        <i className="fa fa-map"></i>
                                    </span>
                                    <span>Tổng số {160} bài học</span>
                                </div>
                                <div className="icon-text mb-2 course_info__content-basic-item">
                                    <span className="icon has-text-danger mr-4">
                                        <i className="fa fa-leanpub"></i>
                                    </span>
                                    <span>Tổng số {160} bài học</span>
                                </div>
                                <div className="icon-text mb-2 course_info__content-basic-item">
                                    <span className="icon has-text-danger mr-4">
                                        <i className="fa fa-battery-full"></i>
                                    </span>
                                    <span>Học mọi lúc mọi nơi</span>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </HomeLayout>
    );
}

function DropdownLesson() {

    const [show, setShow] = useState(false);

    return (
        <>
            <div className="dropdown_lesson__wrapper">
                <div
                    className="dropdown_lesson__button has-background-white-ter"
                    onClick={() =>
                        setShow(!show)}
                >
                    <div>
                        <span className="icon has-text-danger">
                            <i className="fa fa-plus"></i>
                        </span>
                        <span>1. Bắt đầu</span>
                    </div>
                    <div>5 bài học</div>
                </div>
                {/*content show*/}
                {
                    show &&
                    <div id="myDropdownLesson" className="dropdown_lesson__content">
                        <div className="dropdown_lesson__content-item">
                            <div>
                                <span className="icon has-text-danger">
                                    {/* check file hay video */}
                                    <i className="fa fa-play-circle"></i>
                                </span>
                                <span>1. Bắt đầu</span>
                            </div>
                            <div>40:00</div>
                        </div>
                        <div className="dropdown_lesson__content-item">
                            <div>
                                <span className="icon has-text-danger">
                                    {/* check file hay video */}
                                    <i className="fa fa-file"></i>
                                </span>
                                <span>1. Bắt đầu</span>
                            </div>
                            <div>40:00</div>
                        </div>

                    </div>
                }

            </div>

        </>


    );
}

function RelatedCourseItem() {
    return (
        <div className="related_course__wrapper">
            <img className="related_course__img" src=".././logoCyber.png" />
            <div className="related_course__info">
                <h6 className="subtitle is-6 mb-1">HTML CSS Pro</h6>
                <div className="hero is-flex-direction-row is-justify-content-flex-start is-align-items-center">
                    <p className="related_course__price-old">2.499.000đ</p>
                    <h6 className="related_course__price-new">1.999.000đ</h6>
                </div>
                <div className="related_course__des">
                    Đây là một khóa học gì đó khá ok với mức giá trên trời ơi đất hỡi luôn
                </div>

            </div>

        </div>
    );
}

export default CourseInfo;

