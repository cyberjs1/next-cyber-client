import { useState } from "react";
import { EditorState } from "draft-js";
import dynamic from 'next/dynamic';
const Editor = dynamic(
    () => import('react-draft-wysiwyg').then((mod) => mod.Editor),
    { ssr: false }
)

function NoteOnlyComponent({ isActive, setActiveModel }) {
    const [editorState, setEditorState] = useState(EditorState.createEmpty())

    return (
        <div className={`modal ${isActive ? "is-active" : "modal-hide"}  note_only__wrapper `}>
            <div className="note_only__wrapper-content">
                <div style={{ display: "flex", alignItems: "center", marginBottom: "20px" }}>
                    <div><span className="subtitle subtitle-is-5 mr-4">Thêm ghi chú của bạn tại </span></div>
                    <button class="button is-small is-primary">00:00</button>
                </div>
                <>
                    <div>
                        <Editor
                            editorStyle={{ border: "1px solid #ccc", padding: "0 15px" }}
                            editorState={editorState}
                            placeholder="Nhập ghi chú của bạn ở đây..."
                            toolbarClassName="toolbarClassName"
                            wrapperClassName="wrapperClassName"
                            editorClassName="editorClassName"
                            onEditorStateChange={(editorState) => { setEditorState(editorState) }}
                        />
                    </div>
                    <div className="learning-footer">
                        <button
                            style={{ background: "none", border: "none", padding: "10px 30px" }}
                            onClick={() => setActiveModel(false)}
                        >Hủy</button>
                        <button class="button is-primary is-rounded pl-5 pr-5">Lưu lại</button>
                    </div>
                </>

            </div>
        </div >
    );
}

export default NoteOnlyComponent;