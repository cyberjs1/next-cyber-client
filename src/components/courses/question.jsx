import { useState } from "react";
import { EditorState } from "draft-js";
import dynamic from 'next/dynamic';
const Editor = dynamic(
    () => import('react-draft-wysiwyg').then((mod) => mod.Editor),
    { ssr: false }
)

function QuestionComponent({ isActive, setActiveModel }) {
    const [showNote, setShowNote] = useState(false)
    const [editorState, setEditorState] = useState(EditorState.createEmpty())

    return (
        <div class={`modal ${isActive ? "is-active" : "modal-hide"}  learning-modal `}>
            <div class="learning-modal-content">
                <span onClick={() => setActiveModel(false)} class="learning-modal-close">&times;</span>
                <div className="component-content">
                    <div className="learning-header">
                        <div><h1 class="title is-5">100 hỏi đáp</h1></div>
                    </div>

                    <div className="learning-body" id="scroll-bar">
                        <div
                            className="learning-question-header">
                            <div
                                className="learning-question-header-avatar">
                                <i style={{ fontSize: "40px", }} class="fa fa-user"></i>
                            </div>

                            <div style={{ width: "90%", height: "auto" }}>
                                {showNote ?
                                    <div style={{
                                        position: "relative"
                                    }}>
                                        <div>
                                            <Editor
                                                editorStyle={{ border: "1px solid #ccc", padding: "0 15px" }}
                                                editorState={editorState}
                                                placeholder="Nhập ghi chú của bạn ở đây..."
                                                toolbarClassName="toolbarClassName"
                                                wrapperClassName="wrapperClassName"
                                                editorClassName="editorClassName"
                                                onEditorStateChange={(editorState) => { setEditorState(editorState) }}
                                            />
                                        </div>
                                        <div className="learning-footer">
                                            <button
                                                style={{ background: "none", border: "none", padding: "10px 30px" }}
                                                onClick={() => setShowNote(false)}
                                            >Hủy</button>
                                            <button class="button is-primary is-rounded pl-5 pr-5">Lưu lại</button>
                                        </div>
                                    </div>

                                    :
                                    <input
                                        onClick={() => setShowNote(true)}
                                        className="learning-body-inpquestion"
                                        placeholder="bạn có thắc mắc gì trong bài này?"
                                        type="text"></input>

                                }
                            </div>
                        </div>

                        <div className="learning-body-question">
                            <CommentComponent></CommentComponent>
                            <CommentComponent></CommentComponent>


                        </div>
                    </div>


                </div>

            </div>
        </div >
    );
}

function CommentComponent() {
    const [openFormRepL1, setOpenFormRepL1] = useState(false);
    const [openFormRepL2, setOpenFormRepL2] = useState(false);
    const [showCommentRep, setShowCommentRep] = useState(false);

    return (
        <div className="comment_comp__wrapper">
            <div className="comment_comp__header" >
                <div className="comment_comp__header-top">
                    <div
                        className="comment_comp__header-avatar">
                        <img alt="avatar" src=".././logoCyber.png" />
                    </div>

                    <div className="comment_comp__header-content">
                        <h5 className="title is-6 mb-5">Nguyen Hao</h5>
                        <div>gfdfdgh</div>
                    </div>
                </div>
                <div className="comment_comp__header-txtrep">
                    <p
                        onClick={() => { setOpenFormRepL1(true) }}
                        style={{ cursor: "pointer" }} className="has-text-danger mr-4">Trả lời</p>
                    <p>1 tháng trước</p>
                </div>
                {openFormRepL1 &&
                    <RepCommentForm setActiveModel={setOpenFormRepL1}></RepCommentForm>
                }

            </div>
            <span class="icon-text ml-3"
                onClick={() => setShowCommentRep(!showCommentRep)}
            >
                <span>{showCommentRep ? "Ẩn câu trả lời" : `Xem ${3} câu trả lời`}</span>
                <span class="icon">
                    <i class={`fa ${showCommentRep ? "fa-chevron-up" : "fa-chevron-down"}`}></i>
                </span>

            </span>
            {showCommentRep &&
                <div className="comment_comp__rep">
                    <div className="comment_comp__rep-wrapper" >
                        <div className="comment_comp__header-top">
                            <div
                                className="comment_comp__rep-wrapper-avatar">
                                <img alt="avatar" src=".././logoCyber.png" />
                            </div>
                            <div className="comment_comp__rep-wrapper-content">
                                <h5 className="title is-6 mb-5">Nguyen Hao</h5>
                                <div>gfdfdgh</div>
                            </div>
                        </div>
                        <div className="comment_comp__header-txtrep">
                            <p
                                onClick={() => { setOpenFormRepL2(true) }}
                                style={{ cursor: "pointer" }} className="has-text-danger mr-4">Trả lời</p>
                            <p>1 tháng trước</p>
                        </div>
                        {openFormRepL2 &&
                            <RepCommentForm setActiveModel={setOpenFormRepL2}></RepCommentForm>
                        }
                    </div>

                </div>}

        </div>
    )
}

function RepCommentForm({ setActiveModel }) {
    const [editorState, setEditorState] = useState(EditorState.createEmpty())
    return (
        <div className="comment_comp__rep-form">
            <div className="comment_comp__rep-form-header">
                <div
                    className="comment_comp__rep-wrapper-avatar">
                    <img alt="avatar" src=".././logoCyber.png" />
                </div>
                <div style={{ width: "90%" }}>
                    <Editor
                        editorStyle={{ border: "1px solid #ccc", padding: "0 15px" }}
                        editorState={editorState}
                        placeholder="Nhập ghi chú của bạn ở đây..."
                        toolbarClassName="toolbarClassName"
                        wrapperClassName="wrapperClassName"
                        editorClassName="editorClassName"
                        onEditorStateChange={(editorState) => { setEditorState(editorState) }}
                    /></div>
            </div>
            <div className="learning-footer">
                <button
                    style={{ background: "none", border: "none", padding: "10px 30px" }}
                    onClick={() => setActiveModel(false)}
                >Hủy</button>
                <button class="button is-primary is-rounded pl-5 pr-5">Lưu lại</button>
            </div>
        </div>
    )
}

export default QuestionComponent;