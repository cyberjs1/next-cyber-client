import { useState } from "react";

function DropdownLearning() {
    const [show, setShow] = useState(false);

    return (
        <div className="dropdown_learning__wrapper">
            <div
                className="dropdown_learning__button has-background-white-ter"
                onClick={() =>
                    setShow(!show)}
            >
                <div className="dropdown_learning__button-top">

                    <span>1. Bắt đầu</span>
                    <span class="icon has-text-danger">
                        <i class="fa fa-chevron-down"></i>
                    </span>
                </div>
                <div className="dropdown_learning__button-bottom">

                    <span>0/6</span>
                    <span>|</span>
                    <span>18:39</span>
                </div>

            </div>
            {/*content show*/}
            {
                show &&
                <div id="myDropdownLearning" className="dropdown_learning__content">
                    <div className="dropdown_learning__content-item">
                        <div className="dropdown_learning__content-item-top">
                            <span>1. Bạn sẽ làm được gì sau khóa học?</span>
                        </div>
                        <div className="dropdown_learning__content-item-bottom">
                            <span class="icon has-text-danger m-0 p-0">
                                {/* check file hay video */}
                                <i className="fa fa-play-circle icon-learning-item"></i>
                            </span>
                            <span> 40:00</span>

                        </div>
                    </div>
                    <div className="dropdown_learning__content-item">
                        <div className="dropdown_learning__content-item-top">
                            <span>1. Bạn sẽ làm được gì sau khóa học?</span>
                        </div>
                        <div className="dropdown_learning__content-item-bottom">
                            <span class="icon has-text-danger m-0 p-0">
                                {/* check file hay video */}
                                <i className="fa fa-play-circle icon-learning-item"></i>
                            </span>
                            <span> 40:00</span>

                        </div>
                    </div>
                    <div className="dropdown_learning__content-item">
                        <div className="dropdown_learning__content-item-top">
                            <span>1. Bạn sẽ làm được gì sau khóa học?</span>
                        </div>
                        <div className="dropdown_learning__content-item-bottom">
                            <span class="icon has-text-danger m-0 p-0">
                                {/* check file hay video */}
                                <i className="fa fa-file icon-learning-item"></i>
                            </span>
                            <span> 40:00</span>

                        </div>
                    </div>

                </div>
            }

        </div>
    );
}

export default DropdownLearning;