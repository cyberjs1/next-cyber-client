import { useState } from "react";
import { EditorState } from "draft-js";
import dynamic from 'next/dynamic';
const Editor = dynamic(
    () => import('react-draft-wysiwyg').then((mod) => mod.Editor),
    { ssr: false }
)

function NoteComponent({ isActive, setActiveModel }) {
    const [dropdown, setDropdown] = useState(false);
    const [dropdown2, setDropdown2] = useState(false);
    const [showNote, setShowNote] = useState(false)
    const [editorState, setEditorState] = useState(EditorState.createEmpty())

    return (
        <div className={`modal ${isActive ? "is-active" : "modal-hide"}  learning-modal `}>
            <div className="learning-modal-content">
                <span onClick={() => setActiveModel(false)} className="learning-modal-close">&times;</span>
                <div className="component-content">
                    <div className="learning-header">
                        <div><h1 class="title is-5">Ghi chú của tôi</h1></div>
                        <div class="mr-5">
                            <div
                                onClick={() => setDropdown(!dropdown)}
                                class={`dropdown ${dropdown ? "is-active" : ""} ml-3 mr-3`}>
                                <div class="dropdown-trigger">
                                    <button class="button is-small " aria-haspopup="true" aria-controls="dropdown-menu">
                                        <span>Dropdown button</span>
                                        <span class="icon is-small">
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </span>
                                    </button>
                                </div>
                                <div class="dropdown-menu" id="dropdown-menu" role="menu">
                                    <div class="dropdown-content">
                                        <a href="#" class="dropdown-item">
                                            Trong chương hiện tại
                                        </a>
                                        <a class="dropdown-item">
                                            Trong tất cả các chương
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div
                                onClick={() => setDropdown2(!dropdown2)}
                                class={`dropdown ${dropdown2 ? "is-active" : ""} ml-3 mr-3`}>
                                <div class="dropdown-trigger">
                                    <button class="button is-small " aria-haspopup="true" aria-controls="dropdown-menu">
                                        <span>Dropdown button</span>
                                        <span class="icon is-small">
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </span>
                                    </button>
                                </div>
                                <div class="dropdown-menu" id="dropdown-menu" role="menu">
                                    <div class="dropdown-content">
                                        <a href="#" class="dropdown-item">
                                            Trong chương hiện tại
                                        </a>
                                        <a class="dropdown-item">
                                            Trong tất cả các chương
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="learning-body">
                        <div className="learning-body-title">
                            <div className="learning-body-title-left">
                                <button class="button is-small is-danger is-rounded mr-3">00:00</button>
                                <a class="has-text-danger mr-3">Lịch sử tìm kiếm CSS</a>
                                <p class="title is-6">Xây dựng Web Shopee</p>
                            </div>

                            <div className="learning-body-title-right">
                                <button
                                    onClick={() => { setShowNote(!showNote) }}
                                >
                                    <span class="icon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
                                </button>
                                <button>
                                    <span class="icon">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                </button>
                            </div>
                        </div>

                        <div className="learning-body-note">

                            {showNote ?
                                <>
                                    <div>
                                        <Editor
                                            editorStyle={{ border: "1px solid #ccc", padding: "0 15px" }}
                                            editorState={editorState}
                                            placeholder="Nhập ghi chú của bạn ở đây..."
                                            toolbarClassName="toolbarClassName"
                                            wrapperClassName="wrapperClassName"
                                            editorClassName="editorClassName"
                                            onEditorStateChange={(editorState) => { setEditorState(editorState) }}
                                        />
                                    </div>
                                    <div className="learning-footer">
                                        <button
                                            style={{ background: "none", border: "none", padding: "10px 30px" }}
                                            onClick={() => setShowNote(false)}
                                        >Hủy</button>
                                        <button class="button is-primary is-rounded pl-5 pr-5">Lưu lại</button>
                                    </div>
                                </>

                                :
                                <div className="learning-body-note-content">
                                    f
                                </div>

                            }

                        </div>
                    </div>


                </div>

            </div>
        </div >
    );
}

export default NoteComponent;