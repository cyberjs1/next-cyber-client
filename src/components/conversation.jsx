export default function Conversation(props) {
    const { name, img, index, setOpenChat, setIndexUser } = props;
    const setChat = (index) => {
        setIndexUser(index)
        setOpenChat(true)
    }
    return (
        <div className="conversation box m-1 is-flex p-3" onClick={() => setChat(index)}>
            <img src={img ? img : "user/wait-loading.jpg"} className="chat-avatar is-flex-grow-0 my-auto mx-2" alt="" />
            <a className="has-text-black is-block is-flex-grow-0 my-auto mx-2 has-text-primary "><b>{name}</b></a>
        </div>
    )
}