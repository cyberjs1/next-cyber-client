function AccountsManagement() {
  return (
    <div className="accounts_management__wrapper">
      <div className="accounts_management__header">
        <div className="accounts_management__search-box pb-5">
          <input className="input" type="text" placeholder="Search..." />
          <button className="button is-primary">Search</button>
        </div>
        <div className="accounts_management__action">
          <button className="button is-primary is-flex is-align-items-center is-justify-content-center">
            <span> Add new accounts</span>
          </button>
        </div>
      </div>
      <div className="accounts_management__body">
        <table class="table is-fullwidth">
          <thead>
            <th>User id</th>
            <th>Full name</th>
            <th>Email</th>
            <th>Phone number</th>
            <th>Action</th>
          </thead>
          <tbody>
            <tr>
              <td>U123</td>
              <td>Hoang Van Chinh</td>
              <td>chinhhoang@gmail.com</td>
              <td>0999999999</td>
              <td>
                <button className="button">Xoa</button>
                <button className="button">Chat</button>
                <button className="button">Block</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default AccountsManagement;
