import Link from "next/link";
import { useState } from "react";
import Chat from "./chat";
import Conversation from "./conversation";
export default function PopupMessage() {
    const [openPopup, setOpenPopup] = useState(false);
    const [openChat, setOpenChat] = useState(false);
    const [data, setData] = useState([
        { name: "John Doe 0", img: "/user/avatar.png" },
        { name: "John Doe 1", img: "/user/avatar.png" },
        { name: "John Doe 2", img: "/user/avatar.png" },
        { name: "John Doe 3", img: "/user/avatar.png" },
        { name: "John Doe 4", img: "/user/avatar.png" },
        { name: "John Doe 5", img: "/user/avatar.png" },
        { name: "John Doe 5", img: "/user/avatar.png" },
        { name: "John Doe 6", img: "/user/avatar.png" },
        { name: "John Doe 7", img: "/user/avatar.png" },
        { name: "John Doe 8", img: "/user/avatar.png" },
        { name: "John Doe 9", img: "/user/avatar.png" },
        { name: "John Doe 10", img: "/user/avatar.png" },
        { name: "John Doe 11", img: "/user/avatar.png" },
    ]);
    const [indexUser, setIndexUser] = useState(null);
    return (
        <>
            <div className="popup-message has-background-light p-0">
                <div className="header-message is-flex is-flex-direction-row">
                    <span className="icon is-flex-grow-0 m-2">
                        <i className="fa fa-circle has-text-primary"></i>
                    </span>
                    <b className="is-flex-grow-1 m-2">12 online</b>
                    <span className="icon is-flex-grow-0 m-2">
                        <Link href="/messenger"><i className="fa fa-expand has-text-primary"></i></Link>
                    </span>
                    <span className="icon is-flex-grow-0 m-2 " onClick={() => setOpenPopup(!openPopup)}>
                        <i className={"has-text-primary fa fa-caret-square-o-" + (openPopup ? "down" : "up")}></i>
                    </span>
                </div>
                <div className={"content-message m-1 " + (openPopup ? "active" : "")}>
                    <div className={"message-chat " + (openChat ? "" : "list")}>
                        {openChat ? <Chat openChat={openChat} setOpenChat={setOpenChat} user={data[indexUser]} isPopup={true} /> :
                            data.map((item, index) => {
                                return (
                                    <div key={index}>
                                        <Conversation name={item.name} img={item.img} setOpenChat={setOpenChat} index={index} setIndexUser={setIndexUser} />
                                    </div>
                                )
                            })}
                    </div>
                </div>
            </div>
        </>
    )
}