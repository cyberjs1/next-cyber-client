import Link from "next/link"
import { useState } from "react";
import { isAuthenticated } from '../utils/auth';
export default function Nav() {
    const [keyword, setKeyWord] = useState('');
    const [clearText, setClearText] = useState(false);
    const [openNotification, setOpenNotification] = useState(true);
    const [openUserMenu, setOpenUserMenu] = useState(true);
    const clear = () => {
        setKeyWord('')
    }
    const handleKeyword = (evt) => {
        setKeyWord(evt.target.value);
        if (evt.target.value.length !== 0) {
            setClearText(true)
        } else {
            setClearText(false)
        }
    }
    const handlePopup = (e) => {
        if (e === 0 && openNotification) {
            setOpenNotification(!openNotification)
            setOpenUserMenu(true)
        }
        else if (e === 1 && openUserMenu) {
            setOpenUserMenu(!openUserMenu)
            setOpenNotification(true)

        }
        else if (e == 0 && !openNotification) {
            setOpenNotification(!openNotification)
        }
        else {
            setOpenUserMenu(!openUserMenu)
        }
    }
    return (
        <nav className="navbar columns has-shadow  has-text-light mb-0" role="navigation" aria-label="main navigation" style={{ height: '5.5rem' }}>
            <div className="navbar-start navbar-brand column is-pulled-left   is-2-fullhd is-2-widescreen is-2-desktop is-3-tablet is-5-mobile m-1">
                <Link className="navbar-item  m-2" href="/">
                    <img src="../logoCyber.png" alt="Bulma: Free, open source, and modern CSS framework based on Flexbox" width="112" height="28" />
                </Link>
            </div>
            <div className="navbar-end is-pulled-left  column is-4-fullhd is-4-widescreen is-5-desktop is-9-tablet is-hidden-mobile is-left">
                <div className="navbar-item">
                    <div className="field" style={{ width: '100%' }}>
                        <p className="control has-icons-left has-icons-right m-2" style={{ width: '100%' }}>
                            <input
                                className="input is-rounded"
                                type="text"
                                style={{ width: '100%' }}
                                onChange={(evt) => handleKeyword(evt)}
                                value={keyword}
                                placeholder="Enter Your Keyword"
                            />
                            <span className="icon is-small is-left">
                                <i className="fa fa-search"></i>
                            </span>
                            {
                                clearText ?
                                    <span className="icon is-small is-right is-clickable" onClick={() => clear()}>
                                        <i className="fa fa-close has-text-danger"></i>
                                    </span> : <></>
                            }
                        </p>
                    </div>
                </div>
            </div>
            <div className="navbar-end column is-pulled-right  is-2-fullhd is-2-widescreen is-2-desktop is-6-tablet is-6-mobile">
                <div className="navbar-item navbar-end">
                    <div className="field is-grouped">
                        {!isAuthenticated() ?
                            <p className="control">
                                <Link className="button is-rounded is-primary m-2" href={'/login'}>
                                    <span className="icon">
                                        <i className="fa fa-sign-in"></i>
                                    </span>
                                    <span>Login</span>
                                </Link>
                            </p> :
                            <>
                                <Link className="button is-rounded has-text-danger is-outlined m-2" href={'/user/:username/course'}>
                                    <span className="icon">
                                        <i className="fa fa-book has-text-primary"></i>
                                    </span>
                                    <span>Your Course</span>
                                </Link>
                                <div className="dropdown is-active  m-2">
                                    <div className="dropdown-trigger mr-3">
                                        <button className="button is-rounded p-4" aria-haspopup="true" onClick={() => handlePopup(0)} aria-controls="dropdown-notification">
                                            <span className="icon is-small">
                                                <i className="fa fa-bell" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div className="dropdown-trigger">
                                        <button className="button is-rounded p-4" aria-haspopup="true" onClick={() => handlePopup(1)} aria-controls="dropdown-menu">
                                            <span className="icon is-small">
                                                <i className="fa fa-user" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div className={'dropdown-menu custom-dropdown ' + (openNotification ? 'is-hidden' : '')} onMouseLeave={() => handlePopup(0)} id="dropdown-notification" role="menu">
                                        <div className="dropdown-content">
                                            <a href="#" className="dropdown-item">
                                                Dropdown item s
                                            </a>
                                            <hr className="dropdown-divider" />
                                            <a href="#" className="dropdown-item has-text-centered p-0">
                                                Read All.
                                            </a>
                                        </div>
                                    </div>
                                    <div className={'dropdown-menu custom-dropdown-user ' + (openUserMenu ? 'is-hidden' : '')} onMouseLeave={() => handlePopup(1)} id="dropdown-menu" role="menu">
                                        <div className="dropdown-content">
                                            <a href="#" className="dropdown-item">
                                                Võ Hùng Dương
                                                <br />
                                                @vohungduong
                                            </a>
                                            <hr className="dropdown-divider" />
                                            <a href="#" className="dropdown-item">
                                                <span className="icon">
                                                    <i className="fa fa-sign-in"></i>
                                                </span> Logout
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </>
                        }
                    </div>
                </div>
            </div>
        </nav>
    )
}