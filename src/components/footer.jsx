export default function Footer() {
    return (
        <footer className="footer has-background-black has-shadow is-12">
            <div className=" has-text-centered has-text-grey-light mb-0">
                <p>
                    <strong className="has-text-light">Cyber Learning</strong> powered by CyberJS Team.
                </p>
                <img src="/logoCyber.png" alt="" width={"5%"} className="has-background-light" />
            </div>
        </footer>
    )
}