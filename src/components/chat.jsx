import moment from "moment/moment";
import { useEffect, useState, useRef } from "react";
import 'moment/locale/vi';

export default function Chat(props) {
    const { openChat, setOpenChat, user, isPopup } = props;
    const [scrolled, setScrolled] = useState(false);
    moment.locale('vi');
    const [datas, setDatas] = useState([{
        message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere tempore aliquam pariatur odit libero qui veniam. Nostrum, alias numquam, cupiditate, consectetur adipisci harum necessitatibus praesentium quis eius ea aliquam itaque?",
        time: "Mon Jan 2 15:04:05-0700 MST 2023",
        type: "text",
        role: 0
    },
    {
        message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere tempore aliquam pariatur odit libero qui veniam. Nostrum, alias numquam, cupiditate, consectetur adipisci harum necessitatibus praesentium quis eius ea aliquam itaque?",
        time: "Mon Jan 2 15:04:05-0700 MST 2023",
        type: "text",
        role: 1
    }, {
        message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere tempore aliquam pariatur odit libero qui veniam. Nostrum, alias numquam, cupiditate, consectetur adipisci harum necessitatibus praesentium quis eius ea aliquam itaque?",
        time: "Mon Jan 2 15:04:05-0700 MST 2023",
        type: "text",
        role: 0
    },
    {
        message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere tempore aliquam pariatur odit libero qui veniam. Nostrum, alias numquam, cupiditate, consectetur adipisci harum necessitatibus praesentium quis eius ea aliquam itaque?",
        time: "Mon Jan 2 15:04:05-0700 MST 2024",
        type: "text",
        role: 1
    }, {
        message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere tempore aliquam pariatur odit libero qui veniam. Nostrum, alias numquam, cupiditate, consectetur adipisci harum necessitatibus praesentium quis eius ea aliquam itaque?",
        time: "Mon Jan 2 15:04:05-0700 MST 2023",
        type: "text",
        role: 0
    },
    {
        message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere tempore aliquam pariatur odit libero qui veniam. Nostrum, alias numquam, cupiditate, consectetur adipisci harum necessitatibus praesentium quis eius ea aliquam itaque?",
        time: "Mon Jan 2 15:04:05-0700 MST 2023",
        type: "text",
        role: 1
    },]);
    var chatPopupRef = useRef(null);
    const sendImage = () => {
        selectFile()
    }
    const selectFile = () => {
        const input = document.createElement('input');
        input.type = 'file';
        input.accept = 'image/png, image/jpeg';
        input.onchange = (event) => {
            const d = {
                message: event.target.files[0].name,
                time: moment(),
                type: "img",
                role: 1
            }
            setDatas([...datas, d]);
            setScrolled(false)
            console.log(event.target.files[0]);
        };
        input.click();
    }
    const sendMessage = () => {
        const d = {
            message: document.getElementById("content").innerText,
            time: moment(),
            type: "text",
            role: 1
        }
        setDatas([...datas, d]);
        document.getElementById("content").innerText = '';
        setScrolled(false)
    }
    const scrollToBottom = async () => {
        if (chatPopupRef.current && await openChat && !scrolled) {
            chatPopupRef.current.scrollTop = chatPopupRef.current.scrollHeight;
            setScrolled(true)
        }
    }
    scrollToBottom()
    useEffect(() => {
        scrollToBottom()
    }, []);
    return (
        <div className="chat is-flex is-flex-direction-column">
            <div className="chat-header p-2 is-flex">
                {isPopup ? <span className="icon is-flex-grow-0 my-auto mx-2" onClick={() => { setOpenChat(false) }}>
                    <i className="fa fa-arrow-left"></i>
                </span> : null}
                <img src={user ? user.img : "/user/wait-loading.jpg"} className="chat-avatar is-flex-grow-0 my-auto mx-2" alt="" />
                <a className="has-text-black is-block is-flex-grow-0 my-auto mx-2 has-text-primary"><b>{user ? user.name : "Waiting"}</b></a>
            </div>
            <div className={(isPopup ? "" : "chat-page") + " chat-content p-2 has-background-light"} ref={chatPopupRef}>
                {datas.map((item, index) => {
                    return (
                        <div className="cover-item">
                            <div className={"message-item p-4 mb-5 " + (item.role !== 1 ? "" : "you")} key={index}>
                                {
                                    item.type == 'text' ? <>
                                        {item.message}
                                        <p className="time-message">{moment(item.time).toNow()}</p></>
                                        :
                                        <img src={"/course/" + item.message} alt="image" />
                                }
                            </div>
                        </div>
                    )
                })}
            </div>
            <div className={"chat-input is-flex p-0 "+(isPopup?"":"chat-page")}>
                <p className="message-text p-3 pr-6" contentEditable id="content" />
                <span className="icon is-small is-right mt-1 my-auto absolute " onClick={() => sendImage()}>
                    <i className="fa fa-camera has-text-primary "></i>
                </span>
                <span className="icon is-small is-right mt-1 my-auto absolute1 " onClick={() => {
                    sendMessage()
                }}>
                    <i className="fa fa-paper-plane has-text-primary "></i>
                </span>
            </div>
        </div >
    );
}