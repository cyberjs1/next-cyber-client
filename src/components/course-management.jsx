import { useEffect, useState } from "react";

function CourseManagement() {
  const [activeModel, setActiveModel] = useState(false);

  return (
    <div className="course_page__wrapper">
      <div className="course_page__courses">
        <div className="course_page__header">
          <div className="course_page__search-box">
            <div class="select">
              <select>
                <option>Select dropdown</option>
                <option>With options</option>
              </select>
            </div>
            <div className="control is-loading">
              <input
                className="input w-75"
                placeholder="Search..."
                type="text"
              />
            </div>
          </div>
          <div className="course_page__action">
            <button
              onClick={() => setActiveModel(true)}
              className="button is-primary"
            >
              Add a new course
            </button>
            <button className="button is-primary ml-4">Refresh</button>
            <Model isActive={activeModel} setIsActive={setActiveModel}></Model>
          </div>
        </div>
        <div className="course_page__list-course">
          <div className="is-flex is-justify-content-space-between">
            <h2 className="title is-4">Courses</h2>
            <div className="course_page__pageable">
              <span>1-10 in 200</span>

              <div>
                <span>
                  <i class="fa fa-chevron-left"></i>
                </span>
                <span>
                  <i class="fa fa-chevron-right"></i>
                </span>
              </div>
            </div>
          </div>

          <table class="table is-fullwidth ">
            <thead>
              <tr>
                <th>Course name</th>
                <th>Price</th>
                <th>Create date</th>
                <th>Rate</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              <RowIntable></RowIntable>
              <RowIntable isSelected></RowIntable>
              <RowIntable></RowIntable>
              <RowIntable></RowIntable>
              <RowIntable></RowIntable>
            </tbody>
          </table>
        </div>
      </div>
      <div className="course_page__lessons">
        <div className="is-flex is-align-items-center is-justify-content-space-between	">
          <h2 className="title is-4">Lessons</h2>
          <span className="subtitle is-4">
            <i class="fa fa-plus has-text-primary"></i>
          </span>
        </div>
        <div className="course_page__lessons-list">
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
          <LessonItem></LessonItem>
        </div>
      </div>
    </div>
  );
}

function Model({ isActive, setIsActive }) {
  return (
    <div class={`modal ${isActive ? "is-active" : ""}`}>
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">New courses</p>
          <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
          <form action="" method="post">
            <div class="field">
              <label class="label">Courses name</label>
              <div class="control">
                <input class="input" type="text" placeholder="e.g Alex Smith" />
              </div>
            </div>

            <div class="field">
              <label class="label">Price</label>
              <div class="control">
                <input
                  class="input"
                  type="number"
                  placeholder="1.000.000 VND"
                />
              </div>
            </div>
            <div class="field">
              <label class="label">Description</label>
              <div class="control">
                <textarea
                  class="textarea"
                  placeholder="e.g. Hello world"
                ></textarea>
              </div>
            </div>
            <div class="field file is-right is-info">
              <label class="file-label">
                <input class="file-input" type="file" name="resume" />
                <span class="file-cta">
                  <span class="file-icon">
                    <i class="fa fa-upload"></i>
                  </span>
                  <span class="file-label">Image</span>
                </span>
                <span class="file-name">
                  Screen Shot 2017-07-29 at 15.54.25.png
                </span>
              </label>
            </div>
            <div class="field file is-left is-info">
              <label class="file-label">
                <input class="file-input" type="file" name="resume" />
                <span class="file-cta">
                  <span class="file-icon">
                    <i class="fa fa-upload"></i>
                  </span>
                  <span class="file-label">Thumbnail</span>
                </span>
                <span class="file-name">
                  Screen Shot 2017-07-29 at 15.54.25.png
                </span>
              </label>
            </div>
          </form>
        </section>
        <footer class="modal-card-foot">
          <button class="button is-success">Save changes</button>
          <button onClick={() => setIsActive(false)} class="button">
            Cancel
          </button>
        </footer>
      </div>
    </div>
  );
}

function LessonItem() {
  return (
    <div className=" course_page__lessons-box is-flex  is-align-items-center is-justify-content-space-between ">
      <div className="course_page__lessons-circle is-flex is-align-items-center is-justify-content-center">
        <span>1h</span>
      </div>
      <div className="pl-4 is-flex-grow-1">
        <h6 className="title is-6">Css basic</h6>
        <h6 className="subtitle is-6">10 Task</h6>
      </div>
      <div>
        <i class="fa fa-chevron-right"></i>
      </div>
    </div>
  );
}

function RowIntable({ isSelected }) {
  return (
    <tr className={isSelected == true ? "course_page__is-selected-row" : ""}>
      <td>
        <div className="course_page__course-infor">
          <div>
            <figure class=" image is-64x64">
              <img src="https://swiperjs.com/images/sponsors/casinos-hunter.png" />
            </figure>
          </div>
          <div className="course_page__course-name">
            <span className="title is-6">NextJS basic</span>
            <span className="subtitle is-6">10 lessons</span>
          </div>
        </div>
      </td>
      <td>10$</td>
      <td>10/02/2023</td>
      <td>4.8</td>
      <td>
        <button class="button">
          <i class="fa fa-trash has-text-danger"></i>
        </button>
      </td>
    </tr>
  );
}
export default CourseManagement;
