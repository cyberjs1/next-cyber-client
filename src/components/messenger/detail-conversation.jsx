import Link from "next/link";

export default function DetailConversation() {
    return (
        <div className="detail-conversation is-flex is-flex-direction-row p-2">
            <div className="detail-privacy box m-1">
                <div className="profile-privacy has-background-grey m-auto"> <img src="/user/avatar.png" alt="user-profile" className="profile-privacy-img" /></div>
                <Link href={""} className="button has-background-primary button-privacy mt-1">View Profile</Link>
                <button className="button has-background-grey-lighter button-privacy mt-1">Block This User</button>
                <button className="button has-background-danger button-privacy mt-1">Delete This Conversation</button>
            </div>
            <div className="detail-link-conversation box m-1">
                <div className="croll">
                    <a href="" className="button w-100">https://www.facebook.com/abc</a>
                    <a href="" className="button w-100">https://www.facebook.com/abc</a>
                    <a href="" className="button w-100">https://www.facebook.com/abc</a>
                    <a href="" className="button w-100">https://www.facebook.com/abc</a>
                    <a href="" className="button w-100">https://www.facebook.com/abc</a>
                    <a href="" className="button w-100">https://www.facebook.com/abc</a>
                    <a href="" className="button w-100">https://www.facebook.com/abc</a>
                </div>
            </div>
            <div className="detail-image-conversation box m-1">

                <div className="detail-item px-1 py-1"><img src="/user/avatar.png" alt="" /></div>
                <div className="detail-item px-1 py-1"><img src="/user/avatar.png" alt="" /></div>
                <div className="detail-item px-1 py-1"><img src="/user/avatar.png" alt="" /></div>
                <div className="detail-item px-1 py-1"><img src="/user/avatar.png" alt="" /></div>
                <div className="detail-item px-1 py-1"><img src="/user/avatar.png" alt="" /></div>
                <div className="detail-item px-1 py-1"><img src="/user/avatar.png" alt="" /></div>
            </div>
        </div>
    )
}