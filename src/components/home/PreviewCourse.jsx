import dynamic from 'next/dynamic';

const dataCourse = [
    {
        "title": "Basic JavaScript",
        "img": "/course/1.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "Basic HTML & CSS",
        "img": "/course/2.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "Responsive Website",
        "img": "/course/3.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "JavaScript Pro",
        "img": "/course/4.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "HTML & CSS Pro",
        "img": "/course/5.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "Node & Express",
        "img": "/course/6.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "Basic Code",
        "img": "/course/7.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "React JS Pro",
        "img": "/course/8.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "C++ Basic To Advance",
        "img": "/course/9.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "Unbutu",
        "img": "/course/10.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },
    {
        "title": "JavaScript Advance",
        "img": "/course/12.png",
        "className": "box p-0 mb-1",
        "width": 460,
        "height": 260
    },

]
const LazyCourse = dynamic(() => import('./LazyCourse'));
export default function PreviewCourse() {
    return (
        <>
            <p className="has-text-weight-bold">Some Course</p>

            <div className="colums is-flex is-flex-direction-row is-flex-wrap-wrap p-5">
                {
                    dataCourse.map((item, key) => {
                        return (
                            <LazyCourse src={item.img} width={item.width} height={item.height} alt={item.title} key={key} />
                        )
                    })
                }
            </div>
        </>
    )
}