const data = [
    {
        "title": "Thành Quả Của Học Viên",
        "content": "Để đạt được kết quả tốt trong mọi việc ta cần xác định mục tiêu rõ ràng cho việc đó. Học lập trình cũng không ngoại lệ!",
        "background": "linear-gradient(to right,#308276,#00d1b2)",
        "img": "/carousel/Banner_01.png"
    },
    {
        "title": "CyberJS Trên Youtube",
        "content": "CyberJS được nhắc tới ở mọi nơi, ở đâu có cơ hội việc làm cho nghề IT và có những con người yêu thích lập trình F8 sẽ ở đó!",
        "background": "linear-gradient(to right,#ff0000,#d1bd00)",
        "img": "/carousel/Banner_02.png"
    },
    {
        "title": "Học React JS miễn phí!",
        "content": "Khóa học ReactJS từ cơ bản tới nâng cao. Kết quả của khóa học này là bạn có thể làm hầu hết các dự án thường gặp với ReactJS!",
        "background": "linear-gradient(to right,#0056ff,#00a2d1)",
        "img": "/carousel/Banner_03.png"
    },
    {
        "title": "CyberJS Trên Facebook",
        "content": "CyberJS được nhắc tới ở mọi nơi, ở đâu có cơ hội việc làm cho nghề IT và có những con người yêu thích lập trình F8 sẽ ở đó!",
        "background": "linear-gradient(to right,#0056ff,#ca00f1,#fab47e)",
        "img": "/carousel/Banner_04.png"
    }
]

export default function Carousel() {

    return (
        <div className="carousel" style={{ width: '100%' }} aria-label="Gallery">
            <ol className="carousel__viewport ">
                {data.map((item, key) => {
                    return (
                        <li id="carousel__slide1" key={key}
                            tabIndex="0"
                            className="carousel__slide is-pulled-left">
                            <div className="carousel__snapper column is-12 is-flex p-0" style={{
                                background: item.background
                            }}>
                                <div className="carousel-content is-7 has-text-light">
                                    <h1><b>{item.title}</b></h1>
                                    <p>{item.content}</p>
                                </div>
                                <div className="carousel-image column is-5 is-right">
                                    <img src={item.img} height={'100%'} alt="" />
                                </div>
                            </div>
                        </li>
                    )
                })}

            </ol>
        </div >
    );
}