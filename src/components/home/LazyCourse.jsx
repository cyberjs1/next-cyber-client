export default function LazyCourse(props) {
  return (

    <div className="column is-3-desktop is-12-mobile is-relative p-1 m-2 mb-6" style={{ width: props.width - 50, height: props.height - 50 }}>
      <a href="#" className="m-0 p-0 has-text-primary-dark">
        <img src={props.src} alt={props.alt} className="box p-0 mb-1" layout="fill" objectfit="cover" />
        <p className="has-text-weight-bold">{props.alt}</p>
      </a>
    </div>
  );
}