import axios from "axios";
import queryString from "query-string";

const axiosClient = axios.create({
  baseURL: "https://63989079fe03352a94d5c67c.mockapi.io/api",
  headers: {
    "content-type": "application/json",
  },
  paramsSerializer: {
    serialize: (params) => queryString.stringify(params),
  },
});
axiosClient.interceptors.request.use(async (config) => {
  return config;
});
axiosClient.interceptors.response.use(
  (res) => {
    if (res && res.data) {
      return res.data;
    }
    return res;
  },
  (error) => {
    throw error;
  }
);
export default axiosClient;
